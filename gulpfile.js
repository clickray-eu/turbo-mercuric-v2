'use strict';

var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    cleanCSS     = require('gulp-clean-css'),
    htmlmin      = require('gulp-htmlmin'),
    rename       = require('gulp-rename'),
    uglify       = require('gulp-uglify'),
    browserSync  = require('browser-sync'),
    sourcemaps   = require('gulp-sourcemaps'),
    clean        = require('gulp-clean'),
    autoprefixer = require('gulp-autoprefixer'),
    include      = require('gulp-include'),
    fileinclude  = require('gulp-file-include'),
    gutil        = require('gulp-util'),
    path         = require('path'),
    watch        = require('gulp-watch'),
    rename       = require('gulp-rename'),
    concat       = require('gulp-concat-util'),
    download     = require('gulp-download'),
    replace      = require('gulp-replace'),
    minify       = require('gulp-minifier'),
    minifyCss    = require("gulp-minify-css"),
    autoprefixer = require('gulp-autoprefixer');
    
/*
* global variables
*/
var modules_dir_css  = ['src/scss/custom-modules/**/*.scss'];
var modules_dir_js   = ['src/js/custom-modules/**/*.js'];
var template_dir_css = ['src/scss/templates/**/*.scss'];
var template_dir_js  = ['src/js/templates/**/*.js'];
var js_dir           = ['src/js/base/**/*.js', 'src/js/components/**/*.js', 'src/js/layout/**/*.js'];
var css_dir          = ['src/scss/base/**/*.scss', 'src/scss/components/**/*.scss', 'src/scss/helpers/**/*.scss', 'src/scss/layout/**/*.scss', 'src/scss/utils/**/*.scss', 'src/scss/assets/**/*.scss'];
var css_lib          = ['src/scss/vendors/**/*.scss'];
var js_lib           = ['src/js/vendors/**/*.js'];
var module_config    = ['config.json'];
/*
* ###################################
*/

/*
* Identify user
*/
var user=process.argv[3];
    if(user==undefined) {
        user="global";
    }
/*
* ###################################
*/

/*
* Indentify version, Change dev version
*/
var dev_version = process.argv[5];
    if(dev_version == true || dev_version == undefined) {
        dev_version = true;
    } else if(dev_version == false) {
        dev_version = false;
    }
    changeDeveloperVersion();

// Change developer version
function changeDeveloperVersion() {
    let fs = require('fs');
    fs.readFile('./src/scss/base/_variables.scss', (err, data)=>{
        data = data.toString();
        let searchVersion = new RegExp("[$]dev_version[ ]*[:][ ]*([a-z]+)[;]", "g");
        data = data.replace(searchVersion, "$dev_version : " + dev_version + ";")
        fs.writeFile('./src/scss/base/variables.scss', data, function(){})
    })
}
/*
* ###################################
*/
console.log("");
console.log("");
console.log("###################################################");
console.log('##PLIKI BEDA GENEROWANE W KATALOGU : build/'+user+'##');
console.log("###################################################");
/*
* ###################################
*/

/*
* Reload
*/
gulp.task('reload', () => {
    browserSync.reload();
})
/*
* ###################################
*/

/*
* Serve
*/
gulp.task('serve', () => {
    browserSync.init({
        server: {
             baseDir: 'build/'+user+'/',
             files: ['./build/'+user+'/**/*.css','./build/'+user+'/**/*.js']
        },
        open: false
    })
})
/*
* ###################################
*/

/*
* Clean
*/
gulp.task('clean', ['default'], () => {
    gulp.src(['./build/' + user])
        .pipe(clean(
            {
                force : true, 
                read : true
            }
        ));
})
/*
* ###################################
*/

/*
* Create or download index.html
*/
gulp.task('module_config', () => {
    let fs = require('fs');
    let downloadHtml =  (config_json) => {
        download(config_json.download_url)
            .pipe(rename("index2.html"))
            .pipe(replace("/cdn-cgi/","http://www.cloudflare.com/cdn-cgi/"))
            .pipe(replace("/hs/hsstatic/", "https://preview.hs-sites.com/hs/hsstatic/"))
            .pipe(replace("/_hcms/preview/","https://preview.hs-sites.com/_hcms/preview/"))
            .pipe(gulp.dest('build/'+user+'/'));
    }
    fs.readFile('./config.json', (err, data) => {
        if(err == null){
            let config_json=JSON.parse(data.toString());
            let replace_js= new RegExp("src[=][\"\'](.*?)("+config_json.js_replace+")[^\"\']*[\"\']","g");
            let replace_css= new RegExp("href[=][\"\'](.*?)("+config_json.css_replace+")[^\"\']*[\"\']","g");
            downloadHtml(config_json);

        } else {
            let config_json = { 'download_url' : 'http://example.com' };
            fs.writeFile('./config.json', JSON.stringify(config_json), (err) =>{
                downloadHtml(config_json);
            })       
        }
    });
})
/*
* ###################################
*/

/*
* Download information from build folder
*/
gulp.task('recursiveDirectoryFiles', ()=> {
    let fs = require('fs');
    let path = require('path');
    let walk = (dir, done) => {
    let results = [];
    fs.readdir(dir, (err, list) => {
        if (err) return done(err);
            let pending = list.length;
        if (!pending) return done(null, results);
            list.forEach((file) => {
                file = path.resolve(dir, file);
                fs.stat(file, (err, stat) => {
                    if (stat && stat.isDirectory()) {
                        walk(file, (err, res) => {
                            results = results.concat(res);
                            if (!--pending) done(null, results);
                        });
                    } else {
                        let document=file.split("/build/" + user + "/");
                        results.push(document[1]);
                        if (!--pending) done(null, results);
                    }
                });
            });
        });
    };
    walk('./build/' + user, (err, results)=> {
        if (err) throw err;
        xyz(results);
      });
})

//Function for recursiveDirectoryFiles task
function xyz(results) {
    var fs = require('fs');
    
    fs.readFile('./build/' + user + '/index2.html', (err, data)=> {
        var replace;
        data = data.toString();
        results.forEach( function(file) {
            if(file.search('.css')  >-1 ) {
                replace = new RegExp("href[=][\"\'](.*?)(" + file.replace("/","\/") + ")[^\"\']*[\"\']","g");
                data = data.replace(replace, "href='" + file + "'")
            } else {
                replace = new RegExp("src[=][\"\'](.*?)(" + file.replace("/","\/") + ")[^\"\']*[\"\']","g")
                data = data.replace(replace, "src='" + file + "'")
            }
        }) 
        fs.writeFile('./build/' + user + '/index.html', data, function(){});
    })
}
/*
* ###################################
*/

/*
* Modules convert sass and js
*/
gulp.task('modules_dir_css',  () => {
    var versionDate = new Date();
    gulp.src(modules_dir_css)
        .pipe(rename( (path) => {
            path.dirname=path.dirname+"/css";
        }))
        .pipe(sass())
        .pipe(replace("/*==",""))
        .pipe(replace("==*/",""))
        .pipe(autoprefixer({ browsers: ['>0%']}))
        .pipe(gulp.dest('build/' + user + '/modules/'))
        .pipe(rename( (path) => {
            path.extname=".min"+path.extname;
        }))
        .pipe(minifyCss())
        .pipe(gulp.dest('build/' + user + '/modules/'))
        .pipe(browserSync.stream({match: '/modules/**/*.css'}));
});

gulp.task('modules_dir_js', () => {
    var versionDate = new Date();
    gulp.src(modules_dir_js)
        .pipe(rename( (path) => {
            path.dirname=path.dirname + "/js";
        }))
        .pipe(gulp.dest('build/' + user + '/modules/'))
        .pipe(rename( (path) => {
            path.extname=".min"+path.extname;
        }))
        .pipe(uglify())
        .pipe(gulp.dest('build/' + user + '/modules/'))
})
/*
* ###################################
*/

/*
* Templates convert sass and js
*/
gulp.task('template_dir_css', () => {
    var versionDate = new Date();
    gulp.src(template_dir_css)
        .pipe(rename( (path) => {
            path.dirname=path.dirname+"/css";
        }))
        .pipe(sass())
        .pipe(replace("/*==",""))
        .pipe(replace("==*/",""))
        .pipe(autoprefixer({ browsers: ['>0%']}))
        .pipe(gulp.dest('build/' + user + '/templates/'))
        .pipe(rename( (path) => {
            path.extname=".min"+path.extname;
        }))
        .pipe(minifyCss())
        .pipe(gulp.dest('build/' + user + '/templates/'))
        .pipe(browserSync.stream({match: '/templates/**/*.css'}));
});

gulp.task('template_dir_js', () => {
    var versionDate = new Date();
    gulp.src(template_dir_js)
    .pipe(rename( (path) => {
        path.dirname=path.dirname+"/js";
      }))
    .pipe(gulp.dest('build/' + user + '/templates/'))
    .pipe(rename( (path) => {
        path.extname=".min"+path.extname;
    }))
    .pipe(uglify())
    .pipe(gulp.dest('build/' + user + '/templates/'))
})
/*
* ###################################
*/


/*
* Compile all js
*/
gulp.task('js_dir', () => {
    var versionDate = new Date();
    gulp.src(js_dir)
        .pipe(rename( (path) => {
            path.dirname=path.dirname+"/js";
        }))    
        .pipe(gulp.dest('build/'+ user + '/assets/'))
        .pipe(rename( (path) => {
            path.extname=".min"+path.extname;
        }))
        .pipe(uglify())
        .pipe(gulp.dest('build/' + user + '/assets/'))
})
/*
* ###################################
*/


/*
* Compile all js
*/
gulp.task('css_dir', () => {
    var versionDate = new Date();
    gulp.src(css_dir)
        .pipe(sass())
        .pipe(replace("/*==",""))
        .pipe(replace("==*/",""))
        .pipe(autoprefixer({ browsers: ['>0%']}))
        .pipe(gulp.dest('build/' + user + '/assets/'))
        .pipe(browserSync.stream())
        .pipe(concat.header('/* file: <%= file.relative %> */\n'))
        .pipe(concat.footer('\n/* end file: <%= file.relative %>*/\n'))
        .pipe(concat('main.css')).pipe(gulp.dest('build/' + user + '/connected/'))
        .pipe(rename( (path) => {
            path.extname=".min"+path.extname;
        }))
        .pipe(minifyCss())
        .pipe(gulp.dest('build/' + user + '/connected/'))
        .pipe(browserSync.stream());
        
})
/*
* ###################################
*/

/*
* Copy all lib
*/
gulp.task('css_lib', () => {
    var versionDate = new Date();
    gulp.src(css_lib)
        .pipe(rename( (path) => {
            path.dirname=path.dirname+"/css";
        }))
        .pipe(sass())
        .pipe(replace("/*==",""))
        .pipe(replace("==*/",""))
        .pipe(autoprefixer({ browsers: ['>0%']}))
        .pipe(gulp.dest('build/' + user + '/vendors/'))
})

gulp.task('js_lib', () => {
    var versionDate = new Date();
    gulp.src(js_lib)
        .pipe(rename( (path) => {
            path.dirname=path.dirname+"/js";
        }))
        .pipe(gulp.dest('build/' + user + '/vendors/'))
});
/*
* ###################################
*/

gulp.task('default', ['modules_dir_css', 'modules_dir_js', 'template_dir_css', 'template_dir_js', 'js_dir', 'css_dir', 'css_lib', 'js_lib', 'module_config', 'serve'], () => {
    watch(modules_dir_css,  () => {
        gulp.start('modules_dir_css');
    }); 
    watch(modules_dir_js, () => {
        gulp.start('modules_dir_js')
    }).on('change', browserSync.reload);
    watch(template_dir_css, () => {
        gulp.start('template_dir_css');
    });
    watch(template_dir_js, () => {
        gulp.start('template_dir_js')
    }).on('change', browserSync.reload);
    watch(js_dir, () => {
        gulp.start('js_dir')
    }).on('change', browserSync.reload);
    watch(css_dir, () => {
       gulp.start('css_dir');
    });
    watch(css_lib, () => {
        gulp.start('css_lib');
    });
    watch(js_lib, () => {
        gulp.start('js_lib');
    });
    watch(module_config, () => {
        gulp.start('module_config');
    });
    watch("./build/"+user+"/index2.html",()=>{
        gulp.start('recursiveDirectoryFiles');
        browserSync.reload();
    });
});